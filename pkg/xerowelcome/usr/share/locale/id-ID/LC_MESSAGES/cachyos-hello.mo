��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �     �  	   �     �     �     �                 	        %     ;     N     \  	   c  
   m     x  h  �     �     �          -                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Budi Ariyanto <fx.budi.ariyanto@gmail.com>, 2017
Language-Team: Indonesian (Indonesia) (https://www.transifex.com/manjarolinux/teams/70274/id_ID/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id_ID
Plural-Forms: nplurals=1; plural=0;
 Tentang Tidak dapat memuat halaman Chat room DOKUMENTASI Pengembangan Donasi Forum Ikut terlibat Rumah INSTALASI Jalankan ketika start Jalankan instalasi Mailing lists PROYEK Baca saya Info rilis BANTUAN Terimakasih telah bergabung di komunitas kami!

Kami, pengembang CachyOS berharap Anda dapat menikmati CachyOS sebagaimana kami menikmati dalam membuatnya. Link dibawah ini akan membantu Anda untuk memulai mengggunakan sistem operasi baru Anda. Jadi nikmatilah semua pengalaman yang akan Anda alami dan jangan segan-segan untuk memberikan feedback kepada kami. Resource web Tampilan awal untuk CachyOS Selamat Datang di CachyOS! Wiki 