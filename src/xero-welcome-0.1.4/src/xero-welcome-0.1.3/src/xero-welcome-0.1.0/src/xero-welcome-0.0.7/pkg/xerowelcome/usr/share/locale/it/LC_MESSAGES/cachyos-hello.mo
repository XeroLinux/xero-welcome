��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �     �  
   �     �     �     �     �  	   �     	               -     A     O     X     `     y    �     �  "   �     �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Pietro Francesco Fontana <cubanspit@gmail.com>, 2017
Language-Team: Italian (https://www.transifex.com/manjarolinux/teams/70274/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 About Impossibile caricare la pagina. Canale IRC DOCUMENTAZIONE Sviluppo Dona Forums Partecipa Home INSTALLAZIONE Esegui all'avvio Avvia installazione Mailing lists PROGETTO Leggimi Informazioni di versione SUPPORTO Benvenuto nella nostra comunità!

Noi sviluppatori di CachyOS ci auguriamo che tu ti diverta a usarlo quanto noi a crearlo. I seguenti link ti aiuteranno a muovere i primi passi nel nuovo sistema operativo. Goditi la nuova esperienza d'uso e non esitare a lasciarci i tuoi commenti. Risorse web Schermata di benvenuto per CachyOS Benvenuto in CachyOS! Wiki 