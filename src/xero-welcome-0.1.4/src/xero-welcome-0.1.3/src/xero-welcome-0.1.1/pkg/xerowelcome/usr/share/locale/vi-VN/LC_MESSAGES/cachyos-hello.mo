��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �     �     �     �     �     �                    )     5     O     i     y     �     �  
   �  �  �     k     �  "   �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-27 20:47+0000
Last-Translator: Horazone Detex <thmmt2017d@gmail.com>, 2018
Language-Team: Vietnamese (Viet Nam) (https://www.transifex.com/manjarolinux/teams/70274/vi_VN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi_VN
Plural-Forms: nplurals=1; plural=0;
 Về... Không thể tải trang. Phòng chat TÀI LIỆU Phát triển Đóng góp Diễn đàn Tham gia vào Trang chủ CÀI ĐẶT Chạy khi khởi động Chạy trình cài đặt Danh sách thư DỰ ÁN Đọc tôi Thông tin bản phát hành HỖ TRỢ Cảm ơn bạn đã tham gia cộng đồng chúng tôi!

Chúng tôi, những lập trình viên của CachyOS, hi vọng rằng bạn sẽ tận hưởng việc sử dụng CachyOS cũng như chúng tôi tận hưởng việc xây dựng nó. Những đường liên kết bên dưới sẽ giúp bạn khởi đầu sử dụng hệ điều hành của bạn, và đừng do dự khi gửi phản hồi của bạn về chúng tôi. Tài nguyên web<br> Màn hình chào mừng CachyOS Chào mừng đến với CachyOS! Wiki 