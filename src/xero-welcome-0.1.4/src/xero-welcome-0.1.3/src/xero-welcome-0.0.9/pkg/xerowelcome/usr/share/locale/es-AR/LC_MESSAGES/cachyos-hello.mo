��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     	   �     �     �     �  
                        $     +     8     K     _     p     y  !   �     �  <  �     �  !   �          -                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-27 20:47+0000
Last-Translator: Germán Leonardo Vaja <german.vaja@gmail.com>, 2018
Language-Team: Spanish (Argentina) (https://www.transifex.com/manjarolinux/teams/70274/es_AR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_AR
Plural-Forms: nplurals=2; plural=(n != 1);
 Acerca de No se puede cargar la página. Sala de chat DOCUMENTACIÓN Desarrollo Donar Foros Involucrarse Inicio INSTALACIÓN Ejecutar al inicio Ejecutar instalador Listas de correo PROYECTO Léeme Información sobre el lanzamiento SOPORTE ¡Gracias por unirte a nuestra comunidad!

Nosotros, los desarrolladores de CachyOS, esperamos que puedas disfrutar de usar CachyOS tanto como disfrutamos crearlo. Los vínculos debajo te ayudarán a iniciarte con tu nuevo sistema operativo. Así que, disfruta la experiencia, y no dudes en enviarnos tu devolución. Recurso web Pantalla de bienvenida de CachyOS ¡Bienvenido a CachyOS! Wiki 