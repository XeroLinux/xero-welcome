��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  �  �     �  8   �     �     �  
   �     �  
             ,     5  !   S     u     �  
   �     �     �     �    �  =    	  '   >	     f	                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Brian Smith <brian.smith@riseup.net>, 2017
Language-Team: Persian (Iran) (https://www.transifex.com/manjarolinux/teams/70274/fa_IR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fa_IR
Plural-Forms: nplurals=1; plural=0;
 راجع به نمی توانیم صفحه را نمایش بدهیم. اتاق چت مستندات توسعه اهداء کردن انجمن درگیر کاری شوید خانه نصب و راه اندازی راه اندازی در شروع نصب راه اندازی لیست پستی پروژه منرا بخوان انتشار اطلاعات پشتیبانی تشکر از شما برای پیوستن به جامعه ما!

ما، مانجارو توسعه دهندگان، امیدواریم که شما با استفاده از مانجارو همان اندازه لذت ببرید که ما لذت بردن از ساختنش. لینک های زیر کمک خواهد کرد که شما را با سیستم عامل جدید خود شروع کونید. بنابراین از تجربه لذت ببرید, و دریغ نکنید با ما نظرات خود را ارسال کونید. صفحه نمایش خوش آمدید برای مانجارو  به مانجارو خوش آمدید! ویکی 