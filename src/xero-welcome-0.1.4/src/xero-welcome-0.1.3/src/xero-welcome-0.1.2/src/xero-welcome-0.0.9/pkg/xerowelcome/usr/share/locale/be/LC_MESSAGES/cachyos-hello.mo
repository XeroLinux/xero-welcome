��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �                    -  7   E     }     �     �     �     �     �     �       =        \     |     �     �  (   �     �  $  �     
  !   /
  )   Q
     {
                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-27 20:47+0000
Last-Translator: Zmicer Turok <nashtlumach@gmail.com>, 2019
Language-Team: Belarusian (https://www.transifex.com/manjarolinux/teams/70274/be/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: be
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Пра праграму Немагчыма загрузіць старонку. Размовы ДАКУМЕНТАЦЫЯ Распрацоўка Ахвяраваць Форумы Далучыцца Хатні каталог УСТАЛЁЎКА Запускаць падчас запуску сістэмы Пачаць усталёўку Адрасы пошты ПРАЕКТ Чытаць Інфармацыя пра выпуск ПАДТРЫМКА Дзякуем за ўдзел у нашай суполцы!

Мы, распрацоўшчыкі CachyOS, спадзяёмся, што вы будзеце атрымліваць такую ж асалоду ад выкарыстання CachyOS, якую атрымліваем мы ад распрацоўкі. Прыведзеныя ніжэй спасылкі дапамогуць вам пачаць працу з новай аперацыйнай сістэмай.Не саромейцеся, дасылайце нам свае водгукі. Сеціўныя крыніцы Экран вітання CachyOS Сардэчна вітаем у CachyOS! Вікіпедыя 