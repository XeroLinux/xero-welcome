��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �  %   �     �     �               !     (     4     <  1   I     {     �     �     �     �     �  9  �                 A     W                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Fábio Caramelo <fabiocaramelo18@gmail.com>, 2017
Language-Team: Portuguese (Portugal) (https://www.transifex.com/manjarolinux/teams/70274/pt_PT/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_PT
Plural-Forms: nplurals=2; plural=(n != 1);
 Sobre Não é possível carregar a página. Sala de bate-papo DOCUMENTAÇÃO Desenvolvimento Doação Fórum Envolver-se Início INSTALAÇÃO Mostrar esta caixa de diálogo na inicialização Iniciar instalação Listas de discussão PROJETO Leia-me Informações de lançamento SUPORTE Obrigado por se juntar à nossa comunidade!

Nós, os desenvolvedores de CachyOS, esperamos que você goste de usar CachyOS tanto quanto nós gostamos de construí-lo. Os links abaixo ajudarão você a começar com seu novo sistema operacional. Aprecie assim a experiência, e não hesite enviar-nos seu feedback. Recurso web Tela de boas-vindas para CachyOS Bem-vindo ao CachyOS! Wiki 