��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     
   �       	   %     /     =     D     L     S     _     h     t     �     �     �  	   �     �     �  \  �  
   S     ^     }     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Milos Kamenovic <stonemanhero@gmail.com>, 2017
Language-Team: Serbian (Serbia) (https://www.transifex.com/manjarolinux/teams/70274/sr_RS/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr_RS
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 O programu Ne mogu da učitam stranicu. Ćaskanje DOKUMENTACIJA Razvoj Doniraj Forumi Uključi se Početak INSTALACIJA Pokreni zajedno sa sistemom Pokreni program za instalaciju Lista za dopisivanje PROJEKAT Pročitaj Informacije o izdanju PODRŠKA Hvala Vam što ste se pridružili našoj zajednici!

Mi, koji radimo na CachyOS-u se nadamo da ćete uživati koristeći ga barem onoliko, koliko smo mi uživali praveći ga. Linkovi ispod će Vam pomoći oko upoznavanja Vašeg novog operativnog sistema. Zato uživajte u korišćenju i ne oklevajte da nam pošaljete Vaša iskustva o radu sa njim. Veb resurs Ekran dobrodošlice za CachyOS Dobrodošli u CachyOS! Viki 