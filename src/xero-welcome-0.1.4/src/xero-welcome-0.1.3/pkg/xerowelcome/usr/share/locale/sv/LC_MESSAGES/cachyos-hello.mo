��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            y        �     �     �     �  
   �     �     �     �     �     �               /     <  
   D     O     [  3  c  
   �     �     �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: ordtrogen <johan@ordtrogen.se>, 2017
Language-Team: Swedish (https://www.transifex.com/manjarolinux/teams/70274/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Kan inte läsa in sidan. Chattrum DOKUMENTATION Utveckling Donera Forum Engagera dig Hem INSTALLATION Kör vid uppstart Kör installationsprogram E-postlistor PROJEKT Läs detta Releaseinfo SUPPORT Tack för att du går med i vår gemenskap!

Vi, CachyOSs utvecklare, hoppas att du kommer att tycka om att använda CachyOS lika mycket som vi tycker om att utveckla det. Länkarna nedan hjälper dig att komma igång med ditt nya operativsystem. Så njut av upplevelsen, och tveka inte att ge oss feedback. Webbresurs Välkomstskärm för CachyOS Välkommen till CachyOS! Wiki 