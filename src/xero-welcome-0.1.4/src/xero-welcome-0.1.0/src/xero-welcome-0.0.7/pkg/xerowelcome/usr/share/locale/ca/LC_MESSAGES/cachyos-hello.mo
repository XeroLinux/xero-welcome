��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  v  �     r     x     �     �     �     �     �     �     �     �     �          &     8  
   A  !   L     n  O  u  !   �     �     �                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: acutbal <acutbal@gmail.com>, 2017
Language-Team: Catalan (https://www.transifex.com/manjarolinux/teams/70274/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Sobre No es pot  carregar la plana. Sala de xat DOCUMENTACIÓ Desenvolupament Donar Fòrums Col.labora amb nosaltres Inici INSTAL.LACIÓ Inicia al començament Inicia instal.lador Llistes de correu PROJECTE Llegeix-me Informació sobre el llançament  SUPORT ¡Gràcies per unir-te a la nostra comunitat!

Nosaltres, els Desenvolupadors de CachyOS, esperem que gaudeixis emprant CachyOS tant com nosaltres en gaudim en el seu desenvolupament.  Els enllaços de sota t'ajudaran a començar amb el teu nou sistema operatiu. Gaudeix de l'experiència, i no dubtis a enviar-nos els teus comentaris. Pantalla de benvinguda de CachyOS ¡Benvingut a CachyOS! Wiki 