��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            y     	   �     �     �     �     �     �     �  
                  "     7     M     a  	   h     r     �  g  �     �           )     A                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Hugo Posnic <huluti@cachyos.org>, 2017
Language-Team: French (https://www.transifex.com/manjarolinux/teams/70274/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Impossible de charger la page. Salon de chat DOCUMENTATION Développement Faire un don Forums S'investir Accueil INSTALLATION Lancer au démarrage Lancer l'installateur Listes de diffusion PROJET Lisez moi Informations de version SUPPORT Merci de vous joindre à notre communauté !

Nous, les développeurs de CachyOS, espérons que vous apprécierez l'utilisation de CachyOS autant que nous aimons la construire. Les liens ci-dessous vous aideront à démarrer avec votre nouveau système d'exploitation. Alors profitez de l'expérience, et n'hésitez pas à nous faire part de vos commentaires. Ressource web Écran de bienvenue pour CachyOS Bienvenue sur CachyOS ! Wiki 