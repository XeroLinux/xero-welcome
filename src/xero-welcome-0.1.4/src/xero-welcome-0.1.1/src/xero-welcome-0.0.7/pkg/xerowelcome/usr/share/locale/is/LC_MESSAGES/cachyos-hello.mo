��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �     �     �     �  
   �                    &  
   +     6     F  
   X     c  	   l     v     �  T  �     �     �          -                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Kristján Magnússon <kristjanmagnus@gmail.com>, 2017
Language-Team: Icelandic (https://www.transifex.com/manjarolinux/teams/70274/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 Um Get ekki hlaðið inn síðu. Spjallsvæði SKJÖL Uppbygging Styrkja Umræðutorg Taktu þátt Heim UPPSETNING Ræsa í byrjun Ræsa uppsetningu Póstlisti VERKEFNI Lestu mig Útgáfuupplýsingar STYÐJA Þakka þér fyrir að taka þátt í samfélagi okkar!

Við, sem CachyOS hönnuðir, vonum að þú munt njóta þess að nota CachyOS eins mikið og við njótum að byggja það. Hlekkirnir hér að neðan munu hjálpa þér að byrja á nýja stýrikerfinu þínu. Svo njóttu reynslunar, og ekki hika við að senda okkur þitt álit. Vef úrræði Byrjunar skjár fyrir CachyOS Velkomin(n) til CachyOS! Wiki 