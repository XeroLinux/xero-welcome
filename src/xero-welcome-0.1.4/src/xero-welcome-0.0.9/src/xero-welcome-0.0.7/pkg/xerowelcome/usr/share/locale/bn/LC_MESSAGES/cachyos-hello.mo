��          L      |       �      �      �      �     �      �  �  �  !   y     �     �  6  �  E                                            DOCUMENTATION PROJECT SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome to CachyOS! Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Rashedul Kabir <rkabir.rashed@gmail.com>, 2017
Language-Team: Bengali (https://www.transifex.com/manjarolinux/teams/70274/bn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bn
Plural-Forms: nplurals=2; plural=(n != 1);
 ডকুমেন্টেশন প্রজেক্ট সহায়তা ধন্যবাদ আমাদের কমিউনিটিতে যুক্ত হওয়ার জন্য

আমরা মানজারো প্রস্তুতকারকরা আশা করছি আপনার কাছে মানজারো ততটাই ভালো লাগবে যতটা আমাদের মানজারো তৈরি করতে ভালো লেগেছে। নিচের লিংকগুলো আপনাকে আপনার নতুন অপারেটিং সিস্টেমের সাথে পরিচিত হতে সাহায্য করবে। এই অভিজ্ঞতা উপভোগ করুন, এবং আমাদের আপনার মতামত জানাতে ভুলবেন না। মানজারোতে আপনাকে স্বাগতম! 