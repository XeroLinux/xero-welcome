# Translation of CachyOS-Hello.
# Copyright (C) 2016-2017 CachyOS Developers <manjaro-dev@cachyos.org>
# This file is distributed under the same license as the CachyOS-Hello package.
# Hugo Posnic <huluti@cachyos.org>, 2016-2017.
# 
# Translators:
# Nikoloz Gochiashvili <nick@fina2.net>, 2017
msgid ""
msgstr ""
"Project-Id-Version: CachyOS-Hello\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-04 10:28+0100\n"
"PO-Revision-Date: 2017-10-07 19:09+0100\n"
"Last-Translator: Nikoloz Gochiashvili <nick@fina2.net>, 2017\n"
"Language-Team: Georgian (https://www.transifex.com/manjarolinux/teams/70274/ka/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ka\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ui/cachyos-hello.glade:55
msgid "Welcome to CachyOS!"
msgstr "კეთილი იყოს თქველი მობრძანება CachyOS-ში"

#: ui/cachyos-hello.glade:71
msgid ""
"Thank you for joining our community!\n"
"\n"
"We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback."
msgstr ""
"მადლობას გიხდით ჩვენს საზოგადოებაში გაწევრიანებისათვის!\n"
"\n"
"ჩვენ, მაჯაროს დეველოპერები, ვიმედოვნებთ რომ ისარგებლებთ მანჯაროს გამოყენებით, როგორც ჩვენ ვქმნით მას. ქვემოთ მოყვანილი ბმულები დაგეხმარებათ თქვენი ახალი ოპერაციული სისტემის დაწყებაში. ასე რომ ისარგებლეთ გამოცდილებით და ნუ მოგერიდებათ გამოგვიგზავნოთ თქვენი გამოხმაურება."

#: ui/cachyos-hello.glade:89
msgid "DOCUMENTATION"
msgstr "დოკუმენტაცია"

#: ui/cachyos-hello.glade:103
msgid "SUPPORT"
msgstr "მხარდაჭერა"

#: ui/cachyos-hello.glade:117
msgid "PROJECT"
msgstr "პროექტი"

#: ui/cachyos-hello.glade:129
msgid "Read me"
msgstr "გაეცანი"

#: ui/cachyos-hello.glade:143
msgid "Release info"
msgstr "გამოშვების ინფორმაცია"

#: ui/cachyos-hello.glade:158
msgid "Wiki"
msgstr "ვიკი"

#: ui/cachyos-hello.glade:163 ui/cachyos-hello.glade:194
#: ui/cachyos-hello.glade:211 ui/cachyos-hello.glade:228
#: ui/cachyos-hello.glade:245 ui/cachyos-hello.glade:262
msgid "Web resource"
msgstr "ვებ რესურსები"

#: ui/cachyos-hello.glade:175
msgid "Get involved"
msgstr "ჩაერთეთ"

#: ui/cachyos-hello.glade:189
msgid "Forums"
msgstr "ფორუმი"

#: ui/cachyos-hello.glade:206
msgid "Chat room"
msgstr "ჩათის ოთახი"

#: ui/cachyos-hello.glade:223
msgid "Mailing lists"
msgstr "საფოსტო სიები"

#: ui/cachyos-hello.glade:240
msgid "Development"
msgstr "განვითარება"

#: ui/cachyos-hello.glade:257
msgid "Donate"
msgstr "შემოწირულობა"

#: ui/cachyos-hello.glade:376
msgid "Launch at start"
msgstr "ჩართვისას გაშვება"

#: ui/cachyos-hello.glade:407
msgid "INSTALLATION"
msgstr "ინსტალაცია"

#: ui/cachyos-hello.glade:419
msgid "Launch installer"
msgstr "ინსტალერის გაშვება"

#: ui/cachyos-hello.glade:462
msgid "Home"
msgstr "მთავარი"

#: ui/cachyos-hello.glade:530 ui/cachyos-hello.glade:544
msgid "About"
msgstr "შესახებ"

#: ui/cachyos-hello.glade:549
msgid "Welcome screen for CachyOS"
msgstr "სტუმარს ეკრანზე CachyOS"

#: src/manjaro_hello.py:212
msgid "Can't load page."
msgstr "გვერდი ვერ ჩიატვირთა."
