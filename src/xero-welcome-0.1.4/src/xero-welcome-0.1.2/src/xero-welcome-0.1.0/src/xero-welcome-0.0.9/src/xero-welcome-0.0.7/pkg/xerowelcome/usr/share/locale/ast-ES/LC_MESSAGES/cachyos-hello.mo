��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     	   �     �     �     �  
   �     �     �                          3     H     [     d     k     �  1  �     �     �     �                                                     
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-27 20:47+0000
Last-Translator: enolp <enolp@softastur.org>, 2019
Language-Team: Asturian (Spain) (https://www.transifex.com/manjarolinux/teams/70274/ast_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ast_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Tocante a Nun pue cargase la páxina. Sala de charra DOCUMENTACIÓN Desendolcu Donar Foros Andechar Aniciu INSTALACIÓN Llanzar nel aniciu Llanzar l'instalador Llistes de corréu PROYEUTU Lleime Información de llanzamientu SOFITU ¡Gracies por xunite a la nuesa comunidá!

Nós, los desendolcadores de CachyOS, esperamos qu'esfrutes usando esta distribución tanto como nós faciéndola. Los enllaces d'embaxo van ayudate a apenzar col sistema operativu, asina qu'esfruta de la esperiencia y nun duldes n'unvianos los tos comentarios. Recursu web Pantalla d'acoyida de CachyOS ¡Afáyate en CachyOS! Wiki 