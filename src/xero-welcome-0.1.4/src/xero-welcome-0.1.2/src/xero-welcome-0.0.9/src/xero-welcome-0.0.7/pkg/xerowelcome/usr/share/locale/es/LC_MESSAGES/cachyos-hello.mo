��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     	   �     �     �     �  
   �     �     �               "     /     ?     P     a     j     p     }  :  �     �  #   �     �                                                     
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Benjamin Perez Carrillo <benperezc@gmail.com>, 2017
Language-Team: Spanish (https://www.transifex.com/manjarolinux/teams/70274/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Acerca de No se puede cargar la página Sala de chat DOCUMENTACIÓN Desarrollo Donar Foros Participa con nosotros Inicio INSTALACIÓN Abrir al inicio Abrir instalador Listas de correo PROYECTO Leeme Información SOPORTE ¡Gracias por unirte a nuestra comunidad!

Nosotros, los Desarrolladores de CachyOS, esperamos que disfrutes usando CachyOS tanto como nosotros disfrutamos construyendolo. Los links de abajo te ayudarán a empezar con tu nuevo sistema opeartivo. Disfruta de la experiencia, y no dudes en enviarnos tus comentarios. Recurso de la red Pantalla de bienvenida para CachyOS Bienvenido a CachyOS Wiki 