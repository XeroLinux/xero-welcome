��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            {        �     �  
   �     �     �     �     �     �     �     �  	             !     /     7     @     M  -  U     �     �     �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Solyd <mijninternet2016@gmail.com>, 2016
Language-Team: Dutch (https://www.transifex.com/manjarolinux/teams/70274/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Over Kan pagina niet laden Chatruimte DOCUMENTATIE Ontwikkeling Doneer Fora Help mee Home INSTALLATIE Autostart Start de installatie Email lijsten PROJECT Lees mij Release info SUPPORT Welkom bij onze community!

Wij, de CachyOS ontwikkelaars, hopen dat je CachyOS met net zoveel plezier gaat gebruiken als wij hebben bij het ontwikkelen ervan. De links hieronder helpen je bij de eerste start van het besturingssysteem. Zo, veel plezier en vergeet ons niet je terugkoppeling te sturen. Web bron Welkomstscherm voor CachyOS Welkom bij CachyOS! Wiki 