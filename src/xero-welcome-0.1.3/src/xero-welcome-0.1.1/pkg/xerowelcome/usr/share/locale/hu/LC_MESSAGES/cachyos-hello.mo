��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            ~        �      �     �     �     �     �     �     �  	             "     >     V     g  	   o     y  
   �  /  �     �      �  !         "                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Lajos Pasztor <mrlajos@gmail.com>, 2017
Language-Team: Hungarian (https://www.transifex.com/manjarolinux/teams/70274/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Névjegy Az oldal betöltése sikertelen. Chat DOKUMENTÁCIÓ Fejlesztés Támogatás Fórum Kapcsolódj be! Kezdőlap TELEPÍTÉS Megjelenítés indításkor A telepítő indítása Levelezőlisták PROJEKT Olvass el Kiadási információ SEGÍTSÉG Mi, a CachyOS fejlesztői reméljük, hogy annyira élvezni fogod a CachyOS használatát, mint mi élveztük az elkészítését. Az alábbi linkek segítenek az első lépésekben az új operációs rendszereddel kapcsolatban. Örömteli élményeket kívánunk és bátran küld el észrevételeidet. Internetes források A CachyOS üdvözlőképernyője Üdvözöl a CachyOS közösség! Wiki 