��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            {     
   �  )   �     �  
   �  
   �  
   �               &  
   8     C     \     y     �     �     �  
   �  �  �     �  "   �  %   �  	   �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Eli Shleifer <eligator@gmail.com>, 2017
Language-Team: Hebrew (https://www.transifex.com/manjarolinux/teams/70274/he/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he
Plural-Forms: nplurals=2; plural=(n != 1);
 אודות לא ניתן לטעון את העמוד. חדר צ'אט תיעוד פיתוח תרומה פורומים הֱיֵה מעורב עמוד הבית התקנה הרץ בעת הפעלה הרצת אשף ההתקנה רשימות דיוור פרויקט קרא אותי הערות מוצר תמיכה תודה שהצטרפתם לקהילה שלנו!

אנו, צוות הפיתוח של מערכת ההפעלה CachyOS, מקווים שתהנו להשתמש ב - CachyOS לפחות באותה המידה שאנו נהנים לפתח אותה. קיצורי הדרך המפורטים מטה יעזרו להתחיל להשתמש במערכת ההפעלה החדשה שלכם. תהנו מהחוויה, ואל תהססו לשלוח לנו משוב. משאב רשת מסך פתיחה עבור CachyOS ברוכים הבאים ל - CachyOS!  ויקי 