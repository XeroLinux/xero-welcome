��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  {  �     w  :   �      �     �     �               ,     ;     H  D   ]  *   �     �     �     �  (        7  `  J  .   �	  !   �	     �	                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Tuffkoll <tuffkollz@gmail.com>, 2017
Language-Team: Bulgarian (https://www.transifex.com/manjarolinux/teams/70274/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Относно Страницата не може да се зареди. Стая за разговори ДОКУМЕНТАЦИЯ Разработка Дарения Форуми Участие Начало ИНСТАЛАЦИЯ Да се отваря при пускане на системата Пускане на инсталатора Пощенски списъци ПРОЕКТ Прочети ме Сведения за изданието ПОДДРЪЖКА Благодарим ви, че се присъединихте към общността ни!

Като разработчици на CachyOS се надяваме, че ще използвате системата със същата радост, с която я създадохме. Препратките по-долу ще ви помогнат да започнете с новата ви операционна система. Насладете се на преживяването и не се притеснявайте да ни изпратите предложения и препоръки. Приветстващ екран на CachyOS Добре дошли в CachyOS! Уикипедия 