��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  }  �  $   y  W   �     �  R     -   _     �     �  '   �     �     �  c     -   }  !   �     �     �  i   �  $   a  =  �  ;   �  7         8                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Can Udomcharoenchaikit <udomc.can@gmail.com>, 2017
Language-Team: Thai (https://www.transifex.com/manjarolinux/teams/70274/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
 เกี่ยวกับเรา ไม่สามารถโหลดเพจที่ต้องการได้ ห้องแชท เอกสารอธิบายรายละเอียดต่างๆ  การพัฒนาซอฟแวร์ บริจาค เว็บบอร์ด การมีส่วนร่วม หน้าโฮม การติดตั้ง ให้เปิดโปรแกรมนี้เมื่อเปิดเครื่อง เริ่มการติดตั้ง เมลลิงลิสต์  โปรเจค อ่านฉัน ข้อมูลของระบบปฎิบัติการรุ่นปัจจุบัน การช่วยเหลือ ขอบคุณที่เข้าร่วมเป็นสมาชิกชุมชนของเรา!


 พวกเรา กลุ่มนักพัฒนาCachyOS หวังว่าท่านจะมีความสุขที่ได้ใช้CachyOSเท่ากับที่พวกเรามีความสุขในการได้สร้างมันขึ้นมา ลิ้งค์ข้างล่างจะช่วยท่านเริ่มหัดใช้ระบบปฏิบัติการอันใหม่ของท่าน ขอให้สนุกกับประสบการณ์การใช้งาน และ อย่าลืมส่งความคิดเห็นของท่านมาให้เรา หน้าต้อนรับสำหรับ CachyOS ยินดีต้อนรับสู่ CachyOS!  วิกิ 