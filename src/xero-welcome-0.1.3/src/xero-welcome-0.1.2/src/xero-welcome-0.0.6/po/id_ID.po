# Translation of CachyOS-Hello.
# Copyright (C) 2016-2017 CachyOS Developers <manjaro-dev@cachyos.org>
# This file is distributed under the same license as the CachyOS-Hello package.
# Hugo Posnic <huluti@cachyos.org>, 2016-2017.
# 
# Translators:
# Nurkholish Ardi Firdaus <crowja.root@gmail.com>, 2017
# Budi Ariyanto <fx.budi.ariyanto@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: CachyOS-Hello\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-04 10:28+0100\n"
"PO-Revision-Date: 2017-10-07 19:09+0100\n"
"Last-Translator: Budi Ariyanto <fx.budi.ariyanto@gmail.com>, 2017\n"
"Language-Team: Indonesian (Indonesia) (https://www.transifex.com/manjarolinux/teams/70274/id_ID/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: id_ID\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ui/cachyos-hello.glade:55
msgid "Welcome to CachyOS!"
msgstr "Selamat Datang di CachyOS!"

#: ui/cachyos-hello.glade:71
msgid ""
"Thank you for joining our community!\n"
"\n"
"We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback."
msgstr ""
"Terimakasih telah bergabung di komunitas kami!\n"
"\n"
"Kami, pengembang CachyOS berharap Anda dapat menikmati CachyOS sebagaimana kami menikmati dalam membuatnya. Link dibawah ini akan membantu Anda untuk memulai mengggunakan sistem operasi baru Anda. Jadi nikmatilah semua pengalaman yang akan Anda alami dan jangan segan-segan untuk memberikan feedback kepada kami."

#: ui/cachyos-hello.glade:89
msgid "DOCUMENTATION"
msgstr "DOKUMENTASI"

#: ui/cachyos-hello.glade:103
msgid "SUPPORT"
msgstr "BANTUAN"

#: ui/cachyos-hello.glade:117
msgid "PROJECT"
msgstr "PROYEK"

#: ui/cachyos-hello.glade:129
msgid "Read me"
msgstr "Baca saya"

#: ui/cachyos-hello.glade:143
msgid "Release info"
msgstr "Info rilis"

#: ui/cachyos-hello.glade:158
msgid "Wiki"
msgstr "Wiki"

#: ui/cachyos-hello.glade:163 ui/cachyos-hello.glade:194
#: ui/cachyos-hello.glade:211 ui/cachyos-hello.glade:228
#: ui/cachyos-hello.glade:245 ui/cachyos-hello.glade:262
msgid "Web resource"
msgstr "Resource web"

#: ui/cachyos-hello.glade:175
msgid "Get involved"
msgstr "Ikut terlibat"

#: ui/cachyos-hello.glade:189
msgid "Forums"
msgstr "Forum"

#: ui/cachyos-hello.glade:206
msgid "Chat room"
msgstr "Chat room"

#: ui/cachyos-hello.glade:223
msgid "Mailing lists"
msgstr "Mailing lists"

#: ui/cachyos-hello.glade:240
msgid "Development"
msgstr "Pengembangan"

#: ui/cachyos-hello.glade:257
msgid "Donate"
msgstr "Donasi"

#: ui/cachyos-hello.glade:376
msgid "Launch at start"
msgstr "Jalankan ketika start"

#: ui/cachyos-hello.glade:407
msgid "INSTALLATION"
msgstr "INSTALASI"

#: ui/cachyos-hello.glade:419
msgid "Launch installer"
msgstr "Jalankan instalasi"

#: ui/cachyos-hello.glade:462
msgid "Home"
msgstr "Rumah"

#: ui/cachyos-hello.glade:530 ui/cachyos-hello.glade:544
msgid "About"
msgstr "Tentang"

#: ui/cachyos-hello.glade:549
msgid "Welcome screen for CachyOS"
msgstr "Tampilan awal untuk CachyOS"

#: src/manjaro_hello.py:212
msgid "Can't load page."
msgstr "Tidak dapat memuat halaman"
