��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  �  �     �  &   �     �            
   -     8     K     X  
   v  +   �     �     �     �     �          #  �  4  &   �  !   �     	                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: abdelhak gasmi <abdelhakg@gmail.com>, 2017
Language-Team: Arabic (https://www.transifex.com/manjarolinux/teams/70274/ar/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 حول لا يمكن تحميل الصفحة. غرفة الشات الوثائق التطوير تبرّع المنتديات شاركنا الصفحة الرئيسية تثبيت أظهر مجدّدا عند التشغيل إفتح المثبِِّت القوائم البريدية المشروع اقرأني معلومات الإصدار المساعدة شكرا لانضمامك لمجتمعنا
نأمل نحن ـ مطوروا منجارو ـ أن تسعد باستعمال منجارو بالقدر الذي سعدنا نحن بتصميمه، ستساعدك الروابط أدناه على البدء مع نظام التشغيل الجديد. إذًا تمتع بالتجربة، ولا تتردد في أن تعلمنا بملاحظاتك. شاشة الترحيب لمنجارو مرحبا بك في منجارو ويكي 