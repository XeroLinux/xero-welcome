��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  z  �  
   v     �     �     �     �     �     �     �     �  	   �               "     :     C     L     h  �  y  $   Z          �                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Arianit Kukaj <akukaj@msn.com>, 2017
Language-Team: Albanian (https://www.transifex.com/manjarolinux/teams/70274/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Rreth nesh Faqja s'mund të ngarkohej. Dhomat e bisedës DOKUMENTACIONI Zhvilluesit Donato Forumi Bashkëangjituni me ne Ballina INSTALIMI Fillo në ndezje Hap instaluesin Lista e e-mail adresave PROJEKTI Më lexo Informatat rreth zhvillimit NA MBËSHTETËNI Falëmenderit që ju bashkangjitët komunitetit !

Ne zhvilluesit e CachyOSs, shpresoj që përdorimi i CachyOS's të jetë aq sa ne përkushtojmë knaqësin tonë ndaj zhvillimit të tij. Lidhjet e më poshtme do t'ju ndihmojnë që ju të keni një fillim më të lehtë më sistemin operativ. Kështu pra që shijojeni eksperiencen, dhe mos ngurroni që të na e ktheni mbeshtetjen apo të na kontaktoni për ndonjë gjë që juve ju shqetëson rreth Sistemit Operativ CachyOS. Mirësevini në fillimin e CachyOS's Mirësevini nga CachyOS ! Wiki 