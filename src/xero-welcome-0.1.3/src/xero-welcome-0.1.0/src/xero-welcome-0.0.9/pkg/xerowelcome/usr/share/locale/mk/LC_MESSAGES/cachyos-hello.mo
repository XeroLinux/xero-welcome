��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �  <   �          +     F     S     b     o     �     �      �  @   �  $        (     5  ,   F     s  d  �     �	  1   �	  (   +
     T
                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Atanas Kostovski <atanasmk16@gmail.com>, 2018
Language-Team: Macedonian (https://www.transifex.com/manjarolinux/teams/70274/mk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
 За програмата Неуспешно вчитување на страната. Чат соба ДОКУМЕНТАЦИЈА Развој Донирај Форуми Вклучи се Дома ИНСТАЛАЦИЈА Вклучи на почеток Вклучи го програмот за инсталација Листи за допишување ПРОЕКТ Прочитај Информации за верзијата ПОДРШКА Ви благодариме што се придруживте на нашата заедница!

Ние, кои работиме на CachyOS проектот, се надеваме дека ќе уживате во користењето барем онолку колку што ние уживавме правејќи го. Линковите подолу ќе ви помогнат да започнете со вашиот нов оперативен систем. Уживајте во употребата, и не се колебајте да ни ги испратите вашите мислења. Веб извор Екран за добредојде на CachyOS Добредојдовте на CachyOS! Вики 