��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  �  �     �  1   �     &     5     P     ]     l     y     �     �  3   �  9   �  $        D     U  &   f     �  K  �  0   �	      
     :
                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Aleksandar Velimirović <velimirovic.aleksandar.1989@gmail.com>, 2016
Language-Team: Serbian (https://www.transifex.com/manjarolinux/teams/70274/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 О програму Не могу да учитам страницу. Ћаскање ДОКУМЕНТАЦИЈА Развој Донирај Форуми Укључи се Почетак ИНСТАЛАЦИЈА Покрени заједно са системом Покрени програм за инсталацију Листа за дописивање ПРОЈЕКАТ Прочитај Информације о издању ПОДРШКА Хвала Вам што сте се придружили нашој заједници!

Ми, који радимо на CachyOS-у се надамо да ћете уживати користећи га барем онолико, колико смо ми уживали правећи га. Линкови испод ће Вам помоћи око упознавања Вашег новог оперативног система. Зато уживајте у коришћењу и не оклевајте да нам пошаљете Ваша искуства о раду са њим. Екран добродошлице за CachyOS Добродошли у CachyOS! Вики 