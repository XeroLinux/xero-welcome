��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  �  �     |     �  	   �     �     �     �     �     �     �     �     �     �     �                    (    /     N     f     |                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Jeff Guo <jeffguorg@gmail.com>, 2017
Language-Team: Chinese (China) (https://www.transifex.com/manjarolinux/teams/70274/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 关于 无法载入页面 聊天室 文档 开发 捐赠 论坛 参与 主页 安装 开机时启动 启动安装程序 邮件列表 项目 说明文档 版本发布信息 支持 谢谢您加入我们的社区！

作为 CachyOS 的开发者，我们希望您能享受使用 CachyOS，就如同我们享受建造它一样。下方的链接将帮助您开始使用您的新操作系统。请尽情享受这新的体验，并请您向我们分享您的意见反馈。 CachyOS 的欢迎页面 欢迎使用 CachyOS! 维基 