��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            W        x  ;   �     �     �     �          .     ;     S     g      �  6   �     �     �       (        F  N  Y     �
  .   �
  +   �
                                                     
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: dsafsadf <heneral@gmail.com>, 2017
Language-Team: Ukrainian (https://www.transifex.com/manjarolinux/teams/70274/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Про програму Неможливо завантажити сторінку. Кімната чату ДОКУМЕНТАЦІЯ Розробка допомога проекту Форуми Брати участь На домашню ВСТАНОВЛЕННЯ Запуск при старті Запуск програми встановлення Списки розсилки ПРОЕКТ Прочитай мене Інформація про випуск ПІДТРИМКА Дякуємо вам за участь у нашій спільноті!

Ми, розробники CachyOS, сподіваємося, що використанням CachyOS, принесе вам таке ж задоволення яке відчували ми створюючи її. Наведені нижче посилання допоможуть вам розпочати роботу з новою операційною системою. Отже, насолоджуйтеся досвідом, і не соромтеся, надсилайте нам свої відгуки. Вебресурс Екран привітання для CachyOS Ласкаво просимо до CachyOS! Вікі 