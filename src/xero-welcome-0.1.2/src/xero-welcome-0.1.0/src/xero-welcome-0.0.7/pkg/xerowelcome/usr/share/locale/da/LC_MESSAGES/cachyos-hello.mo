��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            g        �     �     �     �  	   �     �     �     �     �     �     �     �          '     /     8     G  $  O     t     �     �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-27 20:47+0000
Last-Translator: scootergrisen, 2019
Language-Team: Danish (https://www.transifex.com/manjarolinux/teams/70274/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Om Kan ikke indlæse siden. Chatrum DOKUMENTATION Udvikling Donér Fora Vær med Hjem INSTALLATION Kør ved opstart Kør installationsprogrammet Mailinglister PROJEKT Læs mig Udgivelsesinfo SUPPORT Tak fordi du deltager i vores fællesskab!

CachyOS-udviklerne håber på at du vil vil få lige så meget glæde af at bruge CachyOS som vi gjorde da det blev bygget. Linkene nedenfor hjælper dig i gang med dit nye styresystem. Så nyd oplevelsen, og tøv ikke med at sende os din feedback. Webressource Velkomstskærm for CachyOS Velkommen til CachyOS! Wiki 