��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     
   �     �               '     .     7     >     K  
   Q     \     n     �     �  
   �     �     �  :  �     �     
     $     ;                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: ansich <mojmejlzaforume@gmail.com>, 2017
Language-Team: Slovenian (Slovenia) (https://www.transifex.com/manjarolinux/teams/70274/sl_SI/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl_SI
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 O programu Ni mogoče naložiti strani Klepetalnica DOKUMENTACIJA Razvoj Donacija Forumi Pridruži se Domov NAMESTITEV Zaženi ob zagonu Zaženi program za namestitev Dopisni seznam PROJEKT Preberi me O namestitvi PODPORA Hvala, ker ste se pridružili naši skupnosti! 

CachyOS razvijalci upamo, da boste pri uporabi Manjara uživali enako kot mi pri njegovem razvoju. Spodnje povezave vam bodo pomagale pri začetku uporabe novega operacijskega sistema. Uživajte torej v izkušnji in ne oklevajte s pošiljanjem povratnih informacij. Spletni vir Pozdravno okno za CachyOS Dobrodošli v Manjaru! Wiki 