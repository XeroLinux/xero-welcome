��          �      |      �     �     �  	                   ,     3     :     G     L     Y     i     z     �     �     �     �    �     �     �     �  �  �     �  '   �  	   �     �     �     �     �     �     �     �     �               /     <     L     `  �  g  <   �  4   *     _                                          	                                         
              About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Cybertramp Cybertramp <cybertramp@nate.com>, 2017
Language-Team: Korean (Korea) (https://www.transifex.com/manjarolinux/teams/70274/ko_KR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ko_KR
Plural-Forms: nplurals=1; plural=0;
 About 페이지를 불러올수 없습니다. 채팅방 문서 개발 기부 포럼 참여하기 홈 설치 시스템 시작 시 실행 설치 시작 메일링 목록 프로젝트 읽어주세요 업데이트 정보 지원 우리 커뮤니티에 가입해주셔서 감사합니다!

우리 만자로 개발자들은 여러분이 만자로를 개발하는 것만큼 즐겁게 사용하기를 희망 합니다. 아래의 링크는 당신의 새로운 운영체제를 시작하는 것을 도울 것입니다. 따라서 경험즐기면서 우리에게 주저말고 피드백을 보내주시면 감사하겠습니다. 만자로를 위한 화면에 오신 것을 환영합니다. 만자로 리눅스에 오신것을 환영합니다! 위키 