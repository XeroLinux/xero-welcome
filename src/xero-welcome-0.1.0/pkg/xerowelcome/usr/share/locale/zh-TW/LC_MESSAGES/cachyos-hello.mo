��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �        �     �  	   �     �     �     �     �     �     �     �     �               +     2     9     F    M     d     q     �     �                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Jeff Huang <s8321414@gmail.com>, 2017
Language-Team: Chinese (Taiwan) (https://www.transifex.com/manjarolinux/teams/70274/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
 關於 無法載入頁面。 聊天室 文件 開發 捐款 論壇 參與 首頁 安裝 開機時啟動 啟動安裝程式 郵件清單 專案 讀我 版本資訊 支援 感謝您加入我們的社群！

我們，也就是 CachyOS 開發者，希望您能享受使用 CachyOS 的時光，就如同我們享受建造它一樣。下方的連結將會協助您開始使用您的新作業系統。所以盡情享受並向我們傳送您的回饋吧。 網路資源 CachyOS 的歡迎畫面 歡迎使用 CachyOS! Wiki 