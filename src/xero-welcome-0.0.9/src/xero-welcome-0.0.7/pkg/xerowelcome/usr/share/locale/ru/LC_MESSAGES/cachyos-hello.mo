��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �                    4  9   K     �     �     �     �     �     �     �       &   !     H     [     y     �  &   �     �  B  �     
  /   $
  +   T
     �
                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: Ilya Ostapenko (Jacobtey) <jacobtey@gmail.com>, 2016
Language-Team: Russian (https://www.transifex.com/manjarolinux/teams/70274/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 О программе  Невозможно загрузить страницу. Чат ДОКУМЕНТАЦИЯ Разработка Пожертвовать Форумы Принять участие В начало УСТАНОВКА Запускать при старте Запустить Списки рассылок ПРОЕКТ Читать Информация о выпуске ПОДДЕРЖКА Благодарим Вас за то, что Вы присоединились к нашему сообществу!

Мы, разработчики CachyOS, надеемся, что пользуясь этой системой, Вы будете испытывать такое же удовольствие, какое мы испытывали, создавая ее. Представленные ниже ссылки помогут Вам начать работу. Наслаждайтесь функционалом CachyOS и оставляйте свои отзывы. Веб-ресурс Приветственный экран CachyOS Добро пожаловать в CachyOS! Вики 