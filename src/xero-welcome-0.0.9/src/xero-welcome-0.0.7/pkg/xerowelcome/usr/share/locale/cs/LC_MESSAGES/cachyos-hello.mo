��          �      �           	       	         *     8     D     K     R     _     d     q     �     �     �     �     �     �    �     �     �            �     
   �     �     �     �     �  	   �       
   
       	     "   %     H     Z     x     �     �     �  :  �     �     �          -                                                
      	                                                  About Can't load page. Chat room DOCUMENTATION Development Donate Forums Get involved Home INSTALLATION Launch at start Launch installer Mailing lists PROJECT Read me Release info SUPPORT Thank you for joining our community!

We, the CachyOS Developers, hope that you will enjoy using CachyOS as much as we enjoy building it. The links below will help you get started with your new operating system. So enjoy the experience, and don't hesitate to send us your feedback. Web resource Welcome screen for CachyOS Welcome to CachyOS! Wiki Project-Id-Version: CachyOS-Hello
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-07 19:09+0100
Last-Translator: pavelrz <pavel@rzehak.cz>, 2017
Language-Team: Czech (https://www.transifex.com/manjarolinux/teams/70274/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 O aplikaci Stránku nelze načíst. Diskuzní místnosti DOKUMENTACE Vývoj Přispět Fóra Zapojte se Domů INSTALACE Zobrazit při spuštění systému Spustit instalaci Emailové konference a noviny PROJEKT Čti mě Informace o vydání PODPORA Děkujeme, že jste se přidali k naší komunitě!

Jako vývojáři distribuce CachyOS doufáme, že budete používat CachyOS se stejnou radostí, s jakou jsme ho sestavovali. Odkazy níže vám pomohou začít s vaším novým operačním systémem. Užijte si ho a neostýchejte se poslat nám zpětnou vazbu. Webové zdroje Uvítací obrazovka pro CachyOS Vítejte v systému CachyOS! Wiki 